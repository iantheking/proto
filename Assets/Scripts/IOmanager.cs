﻿using System;
using System.IO;
using UnityEngine;
using Google.Protobuf;

public class IOmanager : MonoBehaviour 
{
    string VectorsPath = "";


    private void Awake()
    {
        SetProtoPath();
    }

    void Start () {}
	

	void Update () {}

    public void SetProtoPath()
    {
        DirectoryInfo di;

        VectorsPath = Path.Combine(Application.streamingAssetsPath, "Vectors");

        if(!Directory.Exists(VectorsPath))
        {
            try { di = Directory.CreateDirectory(VectorsPath); }
            catch { Debug.LogWarning("Protocol buffer path failed to create."); }
        }
    }

    public void UpsertVector2D(Vector2Proto _v2p, string _bufferName)
    {
        string filePath = String.Concat(VectorsPath, "/", _bufferName);
        if(File.Exists(filePath))
        {
            File.Delete(filePath);
        }

        File.WriteAllBytes(filePath, _v2p.ToByteArray());
    }

    public Vector2 RequestVector2D(string _bufferName)
    {
        string filePath = String.Concat(VectorsPath, "/", _bufferName);
        if(File.Exists(filePath))
        {
            byte[] pkg = File.ReadAllBytes(filePath);
            Vector2Proto protoOut = Vector2Proto.Parser.ParseFrom(pkg);
            return new Vector2().FromProto(protoOut);
        }
        return new Vector2();
    }

}
