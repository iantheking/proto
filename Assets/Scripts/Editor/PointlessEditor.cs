﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PointlessManager))]
public class PointlessEditor : Editor
{
    PointlessManager pm;

    public override void OnInspectorGUI()
    {
        pm = (PointlessManager)target;

        if (GUILayout.Button("initialize point list"))
            pm.InitPointList();

        if (GUILayout.Button("serialize"))
            pm.PointListSerialize();

        if (GUILayout.Button("clear"))
            pm.ClearPointList();

        if (GUILayout.Button("deserialize"))
            pm.PointListDeserialize();

        base.OnInspectorGUI();


    }

}
