﻿using UnityEngine;
using UnityEditor;

public class DemoMenu 
{
    [MenuItem("Demo/Setup Demo")]
    public static void DemoSceneSetup()
    {
        SetupScene();
    }

    static void SetupScene()
    {
        GameObject DemoManager = GameObject.Find("DemoManager");

        if(DemoManager==null)
        {
            DemoManager = new GameObject();
        }
        DemoManager.name = "DemoManager";
        IOmanager ioman = DemoManager.GetComponent<IOmanager>();
        if (ioman == null)
        {
            ioman = DemoManager.AddComponent<IOmanager>();
        }

        ioman.SetProtoPath();
        PointlessManager poinman = DemoManager.GetComponent<PointlessManager>();
        if(poinman==null)
        {
            poinman = DemoManager.AddComponent<PointlessManager>();
        }
        poinman.ioman = ioman;
    }

}
