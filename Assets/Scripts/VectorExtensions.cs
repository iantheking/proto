﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google.Protobuf;


public static class VectorExtensions
{
	
	public static Vector2Proto ToProto(this Vector2 _vec2)
	{
		Vector2Proto retVec = new Vector2Proto();
		retVec.X = _vec2.x;
		retVec.Y = _vec2.y;
		return retVec;
	}

	public static Vector3Proto ToProto(this Vector3 _vec3)
	{
		Vector3Proto retVec = new Vector3Proto();
		retVec.X = _vec3.x;
		retVec.Y = _vec3.y;
		retVec.Z = _vec3.z;
		return retVec; 
	}

	public static Vector4Proto ToProto(this Vector4 _vec4)
	{
		Vector4Proto retVec = new Vector4Proto();
		retVec.X = _vec4.x;
		retVec.Y = _vec4.y;
		retVec.Z = _vec4.z;
		retVec.W = _vec4.w;
		return retVec;
	}

	public static Vector2 FromProto(this Vector2 _vec2, Vector2Proto _vecProto)
	{
		_vec2.x = _vecProto.X;
		_vec2.y = _vecProto.Y;
		return _vec2;
	}

	public static Vector3 FromProto(this Vector3 _vec3, Vector3Proto _vecProto)
	{
		_vec3.x = _vecProto.X;
		_vec3.y = _vecProto.Y;
		_vec3.z = _vecProto.Z;
		return _vec3;
	}

	public static Vector4 FromProto(this Vector4 _vec4, Vector4Proto _vecProto)
	{
		_vec4.w = _vecProto.W;
		_vec4.x = _vecProto.X;
		_vec4.y = _vecProto.Y;
		_vec4.z = _vecProto.Z;
		return _vec4;
	}


	public static void TestVector2()
	{
		Vector2 v2 = new Vector2(1f, 1f);
		Vector2Proto vp2 = v2.ToProto();
		Vector2 vc2 = new Vector2().FromProto(vp2);
		v2.FromProto(vp2);
	}

}
