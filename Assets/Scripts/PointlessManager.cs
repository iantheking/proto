﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointlessManager : MonoBehaviour 
{
    public IOmanager ioman;
    public int LenPointList = 100;
    public List<Vector2> PointList = new List<Vector2>();

    public void InitPointList()
    {
        for (int i = 0; i < LenPointList;i++)
        {
            PointList.Add(new Vector2(Random.Range(-50f, 50f),
                                      Random.Range(-50f, 50f)));
        }
    }

    public void ClearPointList()
    {
        PointList = new List<Vector2>();
    }

    public void PointListSerialize()
    {
        for (int i = 0; i < LenPointList;i++)
        {
            ioman.UpsertVector2D(PointList[i].ToProto(),i.ToString());
        }
    }

    public void PointListDeserialize()
    {
        ClearPointList();
        for (int i = 0; i < LenPointList;i++)
        {
            PointList.Add(ioman.RequestVector2D(i.ToString()));
        }
    }
}
